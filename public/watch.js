let peerConnection;
//Connection config
const config = {
  iceServers: [
    {
      urls: ["stun:stun.l.google.com:19302"]
    }
  ]
};

const socket = io.connect(window.location.origin);
const video = document.querySelector("video");

//Response offer request from the broadcaster
socket.on("offer", (id, description) => {
    peerConnection = new RTCPeerConnection(config);
    //Set media description sent from offer request and send the media description of the watcher to the broadcaster via answer request
    peerConnection
      .setRemoteDescription(description)
      .then(() => peerConnection.createAnswer())
      .then(sdp => peerConnection.setLocalDescription(sdp))
      .then(() => {
        socket.emit("answer", id, peerConnection.localDescription);
      });
    //Set the video
    peerConnection.ontrack = event => {
      video.srcObject = event.streams[0];
    };
    //Offer ice candidate  to broadcaster
    peerConnection.onicecandidate = event => {
      if (event.candidate) {
        socket.emit("candidate", id, event.candidate);
      }
    };
  });
  //Add offered ice candidate  from broadcaster
  socket.on("candidate", (id, candidate) => {
  peerConnection
    .addIceCandidate(new RTCIceCandidate(candidate))
    .catch(e => console.error(e));
});

socket.on("connect", () => {
  socket.emit("watcher");
});

socket.on("broadcaster", () => {
  socket.emit("watcher");
});

window.onunload = window.onbeforeunload = () => {
  socket.close();
  peerConnection.close();
};