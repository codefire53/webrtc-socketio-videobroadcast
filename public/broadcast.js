const peerConnections = {};
//ICE config
const config = {
    iceServers: [
      {
        urls: ["stun:stun.l.google.com:19302"]
      }
    ]
  };

const socket = io.connect(window.location.origin);
const video = document.querySelector("video");

//Media constraints
const constraints = {
    video: {facingMode: "user"}
    //audio: false //mute audio
};

navigator.mediaDevices
  .getUserMedia(constraints)
  .then(stream => {
    video.srcObject = stream;
    socket.emit("broadcaster");
  })
  .catch(error => console.error(error));


socket.on("watcher", id => {
    const peerConnection = new RTCPeerConnection(config);
    peerConnections[id] = peerConnection;
    //Stream video tracks to joined watcher
    let stream = video.srcObject;
    stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));
    //Exchange connection information through ice
    peerConnection.onicecandidate = event => {
      if (event.candidate) {
        socket.emit("candidate", id, event.candidate);
      }
    };
    //Information exchange about media of the broadcaster (offer)
    peerConnection
        .createOffer()
        .then(sdp => peerConnection.setLocalDescription(sdp))
        .then(() => {
            socket.emit("offer", id, peerConnection.localDescription);
      });
});
//Set the media information of all watchers
socket.on("answer", (id, description) => {
    peerConnections[id].setRemoteDescription(description);
});
//Add ice candidate of all watchers
socket.on("candidate", (id, candidate) => {
    peerConnections[id].addIceCandidate(new RTCIceCandidate(candidate));
});

//Remove client connection whent a client is disconnected
socket.on("disconnectPeer", id => {
peerConnections[id].close();
delete peerConnections[id];
});

//Close connection before closing the window
window.onunload = window.onbeforeunload = () => {
    socket.close();
};